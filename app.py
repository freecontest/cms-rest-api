import connexion
import prance
from typing import Any, Dict
from pathlib import Path

from cms_rest_api import utils

app = connexion.App(__name__)


@app.app.teardown_appcontext
def close_db(err):
    utils.close_db()


def get_bundled_specs(main_file: Path) -> Dict[str, Any]:
    parser = prance.ResolvingParser(str(main_file.absolute()),
                                    lazy = True, strict = True)
    parser.parse()
    return parser.specification


app.add_api(get_bundled_specs(Path("openapi/main.yaml")),
            resolver = connexion.RestyResolver("cms_rest_api"))
app.run(server = "tornado", host = "0.0.0.0", port = 5000)
