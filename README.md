# cms-rest-api

## Motivation
At Free Contest, we extensively use CMS as a platform for organizing competitive
programming matches. We've also built a multitude of tools that builds around
 CMS and complements it. However as of now, there're no official ways to 
 programmatically interact with CMS, other than:

* using the admin interface and manually parsing HTML pages
* using the provided scripts (cmsAddUser, cmsAddAdmin, ...)

Thus, this project aims to provide a REST API interface to allow external 
tools to interact with a CMS instance in a easy and stable way, without having 
to relying on hacky/unofficial methods

## Database structure
Currently we're tracking the database structure of CMS 1.4.
