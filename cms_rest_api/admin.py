#!/usr/bin/env python3

from marshmallow import Schema, fields, post_dump, pre_load, post_load, \
    RAISE, ValidationError
from sqlalchemy.exc import IntegrityError

from cms.db import Admin
from cmscommon.crypto import parse_authentication, build_password

from cms_rest_api.utils import open_db, bad_request_marshmallow, \
    bad_request_sqlalchemy, success_created, success, not_found, \
    update_existing_object


class AdminSchema(Schema):
    id = fields.Integer()
    name = fields.String(required = True)
    username = fields.String(required = True)
    enabled = fields.Boolean(required = True)
    authentication = fields.String(required = True)
    permission_all = fields.Boolean(required = True)
    permission_messaging = fields.Boolean(required = True)

    @post_dump
    def split_authentication(self, data):
        data["authentication_type"], data["authentication_string"] = \
            parse_authentication(data["authentication"])
        del data["authentication"]

        return data

    @pre_load
    def combine_authentication(self, data):
        data["authentication"] = build_password(data["authentication_string"],
                                                data["authentication_type"])
        del data["authentication_string"]
        del data["authentication_type"]

        return data

    @post_load
    def dict_to_admin(self, data):
        return Admin(**data)


def search():
    session = open_db()
    sql_result = session.query(Admin).all()
    schema = AdminSchema(many = True)
    return schema.dump(sql_result)


def get(admin_id: int):
    session = open_db()
    query = session.query(Admin).get(admin_id)
    schema = AdminSchema()
    return schema.dump(query)


def post(body):
    session = open_db()
    schema = AdminSchema()

    try:
        db_object = schema.load(body, partial = ("id",), unknown = RAISE)
    except ValidationError as e:
        return bad_request_marshmallow(e)

    try:
        session.add(db_object)
        session.commit()
    except IntegrityError as e:
        return bad_request_sqlalchemy(e)

    return success_created(db_object.id)


def delete(admin_id: int):
    session = open_db()

    try:
        db_object = session.query(Admin).get(admin_id)
    except IntegrityError as e:
        return bad_request_sqlalchemy(e)

    if db_object is None:
        return not_found(admin_id)

    try:
        session.delete(db_object)
        session.commit()
    except IntegrityError as e:
        return bad_request_sqlalchemy(e)

    return success()


def put(admin_id: int, body):
    session = open_db()

    try:
        db_object = session.query(Admin).get(admin_id)
    except IntegrityError as e:
        return bad_request_sqlalchemy(e)

    if db_object is None:
        return not_found(admin_id)

    schema = AdminSchema()
    updated_data = schema.load(body)
    update_existing_object(db_object, updated_data, schema, ("id", ))

    try:
        session.add(db_object)
        session.commit()
    except IntegrityError as e:
        return bad_request_sqlalchemy(e)

    return success()
