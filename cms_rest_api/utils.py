from typing import Tuple

from connexion.lifecycle import ConnexionResponse
from connexion.problem import problem
from flask import g
from marshmallow import ValidationError, Schema
from sqlalchemy.exc import SQLAlchemyError

from cms.db import Session

# Database related


def open_db():
    if 'db' not in g:
        g.db = Session()
    return g.db


def close_db():
    db = g.pop('db', None)
    if db:
        db.rollback()
        db.close()


def bad_request_marshmallow(e: ValidationError) -> ConnexionResponse:
    status = 400
    title = "Deserialization failure"
    detail = f"Error encountered while deserializing data"
    extras = {
        "extras": e.normalized_messages()
    }

    return problem(status, title, detail, ext = extras)


def bad_request_sqlalchemy(e: SQLAlchemyError) -> ConnexionResponse:
    status = 400
    title = "SQLAlchemy error"
    detail = "Error while executing SQL instruction"
    extras = {
        "extras": {
            "exception": repr(e)
        }
    }

    return problem(status, title, detail, ext = extras)


def success_created(object_id: int) -> ConnexionResponse:
    body = {
        "success": True,
        "id": object_id
    }

    return ConnexionResponse(201, body = body)


def success() -> ConnexionResponse:
    body = {
        "success": True,
    }

    return ConnexionResponse(200, body = body)


def not_found(object_id: int) -> ConnexionResponse:
    status = 404
    title = "Not Found"
    detail = f"Object with id {object_id} does not exist"

    return problem(status, title, detail)


def update_existing_object(old, new, schema: Schema,
                           exclude: Tuple[str]) -> None:
    for field in schema.fields.keys():
        if field in exclude:
            continue
        value = getattr(new, field, None)
        assert (value is not None)
        setattr(old, field, value)
